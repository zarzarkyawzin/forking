const multer = require('multer');
const fs = require('fs');
var path = './uploads';
// var course = require('../app/controllers/course.controllers');
// course.deleteFolderRecursive(path);



const storage =multer.diskStorage({
    destination: (req,file,cb)=>{
        // fs.mkdirSync(path);
        if (!fs.existsSync(path)){
            fs.mkdirSync(path);
        }
        cb(null,path);
    },
    filename: (req,file,cb)=>{
        cb(null,file.originalname);
    }
});

const fileFilter=(req,file,cb)=>{
    if(file.mimetype==='application/zip'){
        cb(null,true);
    }else{
        req.error="wrong file type";
        cb(null,false,req.error);
    }
}
exports.storageshow=multer({
    storage:storage,
    fileFilter: fileFilter,
    // limits:{
    //     fileSize: 1024 * 1024 * 5
    // }
});