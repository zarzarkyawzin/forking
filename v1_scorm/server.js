'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const dbConfig=require('./config/database.config');
const mongoose=require('mongoose'),
    swaggerUi=require('swagger-ui-express'),
    swaggerDocument=require('./swagger/swagger.json');

const app=express();
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json())

mongoose.Promise=global.Promise;

mongoose.connect(dbConfig.url,{useNewUrlParser:true})
    .then(()=>{console.log("Successfully connected to Testing1 database")})
    .catch(err=>{
        console.log('Could not connect to the database. Exiting now...', err);
        process.exit();
});

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

var route=require('./app/routes/course.routes.js');
app.use('/api',route);
module.exports=app;

const port=3000 || process.env.port;
app.listen(port,()=>{
    console.log(`listening to the ${port}`);
});