var router=require('express').Router();
const courses=require('../controllers/course.controllers');
const pageusage=require('../controllers/pageusage.controllers');
const coursesusage=require('../controllers/courseusage.controllers');
const recordusage=require('../controllers/pagedailyrecord.controllers');

//zzkz Code
router.route('/importCourse')
    .post(courses.importCourse);

router.route('/launchCourse/:courseId/:learnerId')
    .get(courses.launchCourse);

router.route('/editCourse/:courseId')
    .put(courses.editCourse);

router.route('/deleteCourse/:courseId')
    .delete(courses.deleteCourse);

router.route('/courseList')
    .get(courses.getCourseLists);

router.route('/viewCourse/:courseId')
    .get(courses.findonecourse);

router.route('/learnerTotal/:courseId')
    .get(courses.findonecourse);
module.exports=router;
  

// module.exports=(app)=>{
//     const courses=require('../controllers/course.controllers');
//     const pageusage=require('../controllers/pageusage.controllers');
//     const coursesusage=require('../controllers/courseusage.controllers');
//     const recordusage=require('../controllers/pagedailyrecord.controllers');
    

//     app.get('/',courses.show);

//     app.post('/coursess',courses.create);

//     app.get('/coursess',courses.findallcourse);

//     app.get('/coursessearch/:couid',courses.findonecourse);

//     //courseusage
//     app.get('/usage',coursesusage.showusage);

//     app.post('/courseusage',coursesusage.createusage);

//     app.get('/courseusage',coursesusage.findallcourseusage);

//     app.get('/datecourse',coursesusage.getDateCourse);

//     app.get('/learnercoursecount',coursesusage.getlearnercount);

//     app.get('/learnerinfo',coursesusage.getlearnerInfo);

//     //pageusage
//     app.get('/Page',pageusage.showpageusage);

//     app.post('/Pagecreate',pageusage.createpageusage);

//     app.get('/Pages',pageusage.findallpageusage);

//     app.get('/startdate',pageusage.getDatePage);

//     app.get('/learnerspage',pageusage.getlearnerpagecount);


//     //pagerecord

//     app.get('/rec',recordusage.showrecord);

//     app.post('/recreate',recordusage.createrecord);

//     app.get('/records',recordusage.findallrecord);

//     app.get('/recorddate',recordusage.getDateRecord);



// }