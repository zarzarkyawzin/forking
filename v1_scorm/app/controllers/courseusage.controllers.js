const Course=require('../models/courseusage.model');
const json=require('circular-json');

exports.showusage=(req,res)=>{
    res.send('CourseUsage rooot');
}

exports.createusage=(req,res)=>{
    const newCourseu=new Course({
        courseId:req.body.courseId,
        learnerId:req.body.learnerId,
        total_time:req.body.total_time,
        first_access_date:req.body.first_access_date,
        last_access_date:req.body.last_access_date,
        complete_date:req.body.complete_date,
        status: req.body.status,
        progress: req.body.progress
    });

    newCourseu.save()
        .then(savedCourse=>{
            res.send(savedCourse);
        }).catch(err=>{
            res.status(404).send('ERRRRRRROR');
    })
};

exports.findallcourseusage=(req,res)=>{
    Course.find({}).then(coursesu=>{
        res.send(coursesu);
    });
};

exports.getDateCourse=(req,res)=>{
    Course.aggregate([
        {
        $project:{
            
                year: { $year: "$first_access_date" },
                month: { $month: "$first_access_date" },
                 day: { $dayOfMonth: "$first_access_date" },
                hour: { $hour: "$first_access_date" },
                minutes: { $minute: "$first_access_date" },
                seconds: { $second: "$first_access_date" },
                milliseconds: { $millisecond: "$first_access_date" },
                dayOfYear: { $dayOfYear: "$first_access_date" },
                dayOfWeek: { $dayOfWeek: "$first_access_date" }
              
        }
    }]
    // , function (err, result) {
    //     if (err) {
    //         console.log(""+err)
    //     } else {
    //         // console.log(String.toString(result));
    //         res.send(json.stringify(result));
    //     }
    // }
    ).then(data=>{
        res.send(data);
        console.log(data[2].dayOfWeek);
        console.log(data.length);
    }).catch(err=>{
        res.status(500).send({})
    });
};


exports.getlearnercount=(req,res)=>{
    // Course.countDocuments({"courseId":req.body.courseId}).then(data=>{
    //     res.send("learnercount:"+data.toString());
    // })

    Course.aggregate([
        {$group:{
            _id:'$courseId',
            learnercount:{$push:"$learnerId"}
            // "count":{"$sum":1}
        }}
    ]).then(data=>{
        res.send(data);
    });
}

exports.getlearnerInfo=(req,res)=>{
    Course.find({learnerId:req.body.learnerId}).then((data)=>{
        res.send(data);
    })
}