const Course = require('../models/course.model');
const Item = require('../models/item.model');
const CourseUsage = require('../models/courseusage.model');
const PageUsage = require('../models/pageusage.model');
const PageDailyRecord = require('../models/pagedailyrecord.model');
const multer = require('multer');
const filedisk = require('../../filemulter/file.js');

//zzkz Code
const fs = require('fs');
const zlib = require('zlib');
const archiver = require('archiver');
const path = require('path');
const admzip = require('adm-zip');  
var SCORMCloud = require('scormcloud-api-wrapper');
var uuid = require('uuid');
var xml2js = require("xml2js");
var appid= 'MMDB1AVOA0';
var secretkey= 'SH6FKz9hEwLVZ0i6vyYs7oi8HT4hf9Uyi6c6bgvw';
// var multer=require('multer');
// var upload=multer({dest:'/importCourse'});

exports.show=(req,res)=>{
    res.send('RRRRROOOOOTTTTT');
}

const upload = filedisk.storageshow;
const fileupload=upload.single('courseFile');
//zzkz Code
//import the file to the scorm cloud
exports.importCourse = async function(req, res){
    //uploading file
    let file, unzipToDirectory,zipToDirectory, courseTitle, courseDescription,courseId;
    await fileupload(req,res, function(err){
        if(req.error){
            res.send("errrrrrror")
        }else{
            //file path
            file= path.join(req.file.destination,req.file.originalname); 
           // Generate a UUID for courseId
            var extension = path.extname(file);
            courseId = path.basename(file,extension)+""+uuid();

            unzipToDirectory = path.join(req.file.destination, 'ZipFile'); //unzip folder name
            zipToDirectory = path.join(req.file.destination, 'ScormZipFile.zip'); //zip folder name
            courseDescription = req.body.courseDescription;
            courseTitle = req.body.courseTitle;
            
            //extract zip folder
            unzipFolder(file, unzipToDirectory);

            //modify the folder, compress the folder and import to db and scorm cloud
            modifyFolder(unzipToDirectory,zipToDirectory, courseTitle, courseDescription,courseId, false,res);
            
        }
    });
};

var unzipFolder = function (file, unzipToDirectory){
    console.log("unzipfolder");
	try {
	    var zip = new admzip(file);
        zip.extractAllTo(unzipToDirectory);
        console.log("hhhhhh");
        
	}
	catch (exception) {
	    console.error(exception);
	}
};

var deleteFolderRecursive = function(path) {
    if( fs.existsSync(path) ) {
      fs.readdirSync(path).forEach(function(file,index){
        var curPath = path + "/" + file;
        if(fs.lstatSync(curPath).isDirectory()) { // recurse  
          deleteFolderRecursive(curPath);
        } else { // delete file
          fs.unlinkSync(curPath);
        }
      });
      fs.rmdirSync(path);
    }
};

var modifyFolder = function(filepath, zipToDirectory, courseTitle, courseDescription, courseId, edit, res){
    console.log("modifyfolder");
    var addToDirectory = path.join(filepath, 'testing1.js');

	fs.readFile('testing1.js', 'utf8', function (err, data) {
	  if (err) throw err;
	  fs.writeFile(addToDirectory, data, (response,err)=> {
		    if(err) {
		        return console.log(err);
		    }
		    compressFile(filepath, zipToDirectory, courseTitle, courseDescription, courseId, edit, res);
		});
    });
};

var compressFile = function (source, out, courseTitle, courseDescription, courseId, edit, res){
    console.log("compressFile");
	const archive = archiver('zip', { zlib: { level: 9 }});
  	const stream = fs.createWriteStream(out);
  	
  	return new Promise(resolve => {
    	archive
      	.directory(source, false)
      	.on('error', err => reject(err))
      	.pipe(stream);

    	stream.on('close', () => resolve());
    	archive.finalize().then(data=>{
    		importToCloud( source, out, courseTitle, courseDescription, courseId,edit, res);
    	});
  	});
};

var importToCloud = async function (source, zipfile, courseTitle, courseDescription, courseId,edit,res){
	// Create an instance with your API credentials.
	var api = new SCORMCloud(appid, secretkey);
    console.log("importing to cloud");

    //version Control
    let version;
    Course.find({courseId: courseId}).countDocuments()
    .then(data=>{
        version=data;
    })

	//Import a course to your SCORM Cloud
	await api.importCourse(courseId, zipfile, function (error, result) {
        if (error) throw error; console.log(result);
	    //Import to your Mongo database	
		var authorId, createdDate, title;
		api.getAccountInfo(function(error, result){
			if(error) throw error;
			authorId = result.email;
			api.getCourseDetail(courseId, async function(error, result){
				if(error) throw error;
				createdDate = result.createDate;
				title = result.title;
				// Create a course
			    const course = new Course({
			        courseId: courseId, 
			        courseTitle: courseTitle || title,
                    description: courseDescription || null,
                    createDate: createdDate,
                    authorId: authorId,
                    version: version
			    });
			    // Save Course in the database
                course.save();

                 //Save Item in the MongoDB
                fs.readFile(path.join(source,'imsmanifest.xml'),(err,data)=>{
                    var parseString = xml2js.parseString;
                    parseString(data, function (err, result) {
                        let array= result.manifest.organizations[0].organization[0];
                        saveItem(courseId,array, array.title,version);
                    });
                    //delete uploads folder
                    deleteFolderRecursive('uploads');
                    
                });
                
                if(edit==true){
                    //Reset Registration
                    api.getRegistrationListResults (
                        {
                            courseid:courseId
                        },
                        async function(error,result){
                        if (error) throw error; 
                        for(var i=0; i<result.length;i++){
                           await api.resetRegistration (result[i].id,function(error,result){
                                if (error) throw error; 
                                console.log(result);
                            });
                            
                        }
                        res.send("Successfully updated!!!!!!!!!!!!");
                    });
                }
                 
                else res.send("Successfully imported!!!!!!!!!!!!");
			});
		});		
    });
};

var saveItem = function (courseId, array, title, version){
    if(array.item != undefined){
        for(var i=0; i<array.item.length; i++){
            let parentId;
            if(title == array.title) parentId = "";
           else parentId = array.title+"";
           //create an Item
           console.log(parentId);
           const item = new Item({
                courseId: courseId,
                version: version,
                parentId: parentId,
                itemId: array.item[i].$.identifier,
                itemTitle: array.item[i].title+""
            }); 
            //save to Item table
            item.save();
            saveItem(courseId,array.item[i],title,version);
        }
    }
    
};

exports.launchCourse = (req, res)=>{
    //sample data form req
    var courseId, regId, learnerId, version;
    courseId= req.params.courseId;
    learnerId = req.params.learnerId;

    // Create an instance with your API credentials.
    var api = new SCORMCloud(appid, secretkey);	
    api.getCourseDetail(courseId,function(error,result){
        if (error) throw error; 
        version=(result.versions.length)-1;
        CourseUsage.find({
            "courseId" : courseId,
            "learnerId" : learnerId
            }).then(data=>{
                
                if(data=='') {   
                    //creating new registration for the new student
                    regId = uuid();// Generate a UUID for the registration.
    
                    // Create a new registration.
                    api.createRegistration(
                        courseId, 
                        regId,
                        'firstName',
                        'lastName',
                        learnerId,
                    {
                        email: learnerId
                    },
                    function (error, result) {
                        if (error) throw error; 
                        insertCourseUsage(regId,courseId,learnerId,version,res);
                    });
                }
                else {
                    regId= data[0].regId;
                    CourseUsage.find({
                        "regId" : regId,
                        "version" : version
                        }).then(data=>{
                            if(data==''){
                                insertCourseUsage(regId,courseId,learnerId,version,res);
                            }
                            else{
                                api.getRegistrationListResults(
                                    {
                                        courseid: courseId,
                                        learnerid: learnerId
                                    },
                                    function (error, result) {
                                        if (error) throw error; 
                                        //Update CourseUsage to your MongoDB with regId
                                        CourseUsage.updateOne(
                                            {
                                                regId : regId,
                                                version : version
                                            }, 
                                            {
                                                "$set":{ 
                                                    total_time: result[0].registrationreport.totaltime,
                                                    first_access_date: result[0].firstAccessDate,
                                                    last_access_date: result[0].lastAccessDate,
                                                    complete_date: result[0].completedDate,
                                                    status: result[0].registrationreport.complete
                                                }
                                            }
                                            ).then(data=>{
                                                //return url to response
                                                let launchUrl = api.getLaunchUrl(result[0].id, 'blank');
                                                res.send(launchUrl);
                                            });
                                    }
                                  );
                            }
                        });
                    
                }
            });
    });
        
};

var insertCourseUsage = function (regId,courseId, learnerId, version, res){
    // Create an instance with your API credentials.
    var api = new SCORMCloud(appid, secretkey);	
    api.getRegistrationListResults(
        {
        courseid: courseId,
        learnerid: learnerId
        },
        function (error, result) {
            if (error) throw error; 
            let status;
            if(result[0].registrationreport.complete=='unknown') status='false';
            else status='true';
            
            //Insert new CourseUsage to your MongoDB
            const courseusage = new CourseUsage({
                regId: result[0].id,
                courseId: result[0].courseId,
                learnerId: result[0].learnerId,
                version: version,
                total_time: result[0].registrationreport.totaltime,
                first_access_date: result[0].firstAccessDate,
                last_access_date: result[0].lastAccessDate,
                complete_date: result[0].completedDate,
                status: status
            });
            //save to CourseUsage table
            courseusage.save();
           
            //Insert new PageUsage to your MongoDB
            insertPageUsage(regId,version);

            //return url to response
            let launchUrl = api.getLaunchUrl(result[0].id, 'blank');
            res.send(launchUrl);
        }
    );
};

var insertPageUsage = function (regId,version){
    // Create an instance with your API credentials.
    var api = new SCORMCloud(appid, secretkey);	
    api.getRegistrationResult( 
        regId, 
        {
            resultsformat: 'full'
        },
        function (error, result) {
            if(error) throw error;
            if(result.length==undefined){
                recursive1(regId,result,version);
            }
            else{
                // console.log(recursive1(result[0]));
                for( var i=0; i< result.length; i++ ){
                    recursive1(regId,result[i], version);
                }
            }
            
        }
    );
};

function recursive1(regId, array, version){
    if(array.children.activity) array= array.children.activity;
	if(array.id==undefined){
		
		for( var i=0; i<array.length; i++ ){
			if(array[i].children=='') {
                //Insert new PageUsage to your MongoDB
                const pageusage = new PageUsage({
                    itemId: array[i].id,
                    regId: regId,
                    version: version,
                    total_time: array[i].timeacrossattempts,
                    first_access_date: null,
                    last_access_date: null,
                    complete_date: null,
                    status: array[i].completed
                });
                //save to PageUsage table
                pageusage.save();
			}
			else {
				recursive1(regId, array[i].children.activity, version);
			}

		}
	}
	else{
		if(array.children=='') {
            //Insert new PageUsage to your MongoDB
            const pageusage = new PageUsage({
                itemId: array.id,
                regId: regId,
                version: version,
                total_time: array.timeacrossattempts,
                first_access_date: null,
                last_access_date: null,
                complete_date: null,
                status: array.completed
            });
            //save to PageUsage table
            pageusage.save();
        }
		else recursive1(regId, array.children.activity, version);
	}
	// console.log("//////////////");
};

function progressCalculation(regId){
    let progress;
    PageUsage.find({ regId: regId }).countDocuments()
        .then(total=>{
            PageUsage.find({ regId: regId, status:"true"}).countDocuments()
                .then(read=>{
                    progress = (read/total)*100;
                    console.log(progress);
                    
                });
        });
};

exports.editCourse=(req,res)=>{
    let file, unzipToDirectory,zipToDirectory, courseTitle, courseDescription,courseId;
    fileupload(req,res, function(err){
        if(req.error){
            res.send("errrrrrror")
        }else{
            courseId = req.params.courseId;
            courseTitle = req.body.courseTitle;
            courseDescription = req.body.courseDescription;
            file= path.join(req.file.destination,req.file.originalname); //file path
            unzipToDirectory = path.join(req.file.destination, 'ZipFile'); //unzip folder name
            zipToDirectory = path.join(req.file.destination, 'ScormZipFile.zip'); //zip folder name
            
            //extract zip folder
            deleteFolderRecursive(unzipToDirectory);
            unzipFolder(file, unzipToDirectory);

            //modify the folder, compress the folder and import to db and scorm cloud
            modifyFolder(unzipToDirectory,zipToDirectory, courseTitle, courseDescription,courseId,true,res);
            
        }
    });
    
};
exports.test= (req,res)=>{
    fileupload(req,res, function(err){
        console.log(req.file);
    });
};

exports.deleteCourse=(req,res)=>{
    var courseId = req.params.courseId;
    var api = new SCORMCloud(appid, secretkey);

    //delete PageUsage data in MongoDB
    Item.find({courseId: courseId}).then(data=>{
        for(var i=0;i<data.length;i++){
            PageUsage.deleteMany({itemId: data[i].itemId}).then(data=>{});
        } 
    });

    //delete PageDailyRecord data in MongoDB
    Item.find({courseId: courseId}).then(data=>{
        for(var i=0;i<data.length;i++){
            PageDailyRecord.deleteMany({itemId: data[i].itemId}).then(data=>{});
        }
    });

    //delete course data in MongoDB
    Course.deleteMany({courseId : courseId}).then(data=>{
        console.log("delete Course");
    });

    //delete Item data in MongoDB
    Item.deleteMany({courseId: courseId}).then(data=>{
        console.log("delete Item");
    });

    //delete courseUsage data in MongoDB
    CourseUsage.deleteMany({courseId: courseId}).then(data=>{
        console.log("delete CourseUsage");
    });

    //delete data in SCORM Cloud 
    api.deleteCourse(courseId, function(error, result){
        if (error) throw error;
        res.send("Deleted successfully!!!!!!");
    });
    
};// end zzkz Code

exports.getCourseLists=(req,res)=>{
    // Course.find({}).then(courses=>{
    //     res.send(courses);
    // });
    Course.aggregate([
        {
            $lookup:{
                from: "CourseUsage",
                localField: "_id",
                foreignField: "courseId",
                as: "meminfo"
            }
        },{
            $addFields:{
                "registerationlists":{
                    $size: "$meminfo._id"
                }
            }
        },{
            $project:{
                "meminfo": 0
            }
        }
    ]).then(courses=>{
            res.send(courses);
            // console.log(courses);
        });
};

exports.findonecourse=(req,res)=>{
    // console.log(req.params.couid);
    Course.findById(req.params.courseId)
    .then( (cours) =>{
        if(!cours){
            return res.status(404).send({
                message: "Note not found with id 1" + req.params.couid
            }); 
        }
        res.send(cours);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id 2" + req.params.couid
            });                
        }
        return res.status(500).send({
            message: "Error retrieving note with id " + req.params.couid
        });
    });
}

