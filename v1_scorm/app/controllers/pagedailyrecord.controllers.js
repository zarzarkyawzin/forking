const pagerecord=require('../models/pagedailyrecord.model');

exports.showrecord=(req,res)=>{
    res.send('PageRecord rooot');
}

exports.createrecord=(req,res)=>{
    const newrecord=new pagerecord({
        courseId:req.body.courseId,
        itemId:req.body.itemId,
        learnerId:req.body.learnerId,
        total_time:req.body.total_time,
        first_access_date:req.body.first_access_date,
        last_access_date:req.body.last_access_date,
        complete_date:req.body.complete_date
    });

    newrecord.save()
        .then(savedCourse=>{
            res.send(savedCourse);
        }).catch(err=>{
            res.status(404).send('ERRRRRRROR record');
    })
};

exports.findallrecord=(req,res)=>{
    pagerecord.find({}).then(pages=>{
        res.send(pages);
    });
};

exports.getDateRecord=(req,res)=>{

    
    // "11/20/2014 04:11"
    // Pageusage.find({"first_access_date":{"$gte": new Date("2019-06-11T00:00:00.000+00:00"), "$lte": new Date("2019-06-11T08:49:32.000+00:00")}}).then(data=>{
    //     res.send(data);
    // })
    // pagerecord.find({"first_access_date":{"$gte": new Date(req.body.first_access_date), "$lt": new Date(req.body.last_access_date)}}).then(data=>{
    //     res.send(data);
    // });

    pagerecord.aggregate([
        {
            $match:{
                first_access_date:{$gte: new Date(req.body.first_access_date), $lte:new Date(req.body.last_access_date)}
            }
        },
        {   $group:{
            _id:'$itemId',
            learnercount:{$push:"$learnerId"},
            Date:{$push:"$first_access_date"}
            // "count":{"$sum":1}
        }}
    ]).then(data=>{
        res.send(data);
    });
};