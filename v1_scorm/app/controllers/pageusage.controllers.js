const Pageusage=require('../models/pageusage.model');

exports.showpageusage=(req,res)=>{
    res.send('PageUsage rooot');
}

exports.createpageusage=(req,res)=>{
    const newpage=new Pageusage({
        itemId:req.body.itemId,
        learnerId:req.body.learnerId,
        total_time:req.body.total_time,
        first_access_date:req.body.first_access_date,
        last_access_date:req.body.last_access_date,
        complete_date:req.body.complete_date,
        status: req.body.status,
        progress: req.body.progress
    });

    newpage.save()
        .then(savedCourse=>{
            res.send(savedCourse);
        }).catch(err=>{
            res.status(404).send('ERRRRRRROR');
    })
};

exports.findallpageusage=(req,res)=>{
    Pageusage.find({}).then(pages=>{
        res.send(pages);
    });
};

exports.getDatePage=(req,res)=>{

    
    // "11/20/2014 04:11"
    // Pageusage.find({"first_access_date":{"$gte": new Date("2019-06-11T00:00:00.000+00:00"), "$lte": new Date("2019-06-11T08:49:32.000+00:00")}}).then(data=>{
    //     res.send(data);
    // })
    Pageusage.find({"first_access_date":{"$gte": new Date(req.body.first_access_date), "$lt": new Date(req.body.last_access_date)}}).then(data=>{
        res.send(data);
    });
};

exports.getlearnerpagecount=(req,res)=>{
    // Course.countDocuments({"courseId":req.body.courseId}).then(data=>{
    //     res.send("learnercount:"+data.toString());
    // })

    Pageusage.aggregate([
        {$group:{
            _id:'$itemId',
            learnercount:{$push:"$learnerId"}
            // "count":{"$sum":1}
        }}
    ]).then(data=>{
        res.send(data);
    });
}