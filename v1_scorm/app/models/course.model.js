const mongoose=require('mongoose'),
    Schema =mongoose.Schema;

var ObjectId=mongoose.ObjectId;

var CourseSchema=new Schema({
    courseId:{
        type: String
    },
    courseTitle:{
        type: String,
        // minlength:4
    },
    description:{
        type: String,
        // minlength:4
    },
    createDate:{
        type: Date
    },
    authorId:{
        type:String
    },
    version:{
        type: Number,
        default: ()=>0
    }
},{collection: 'Course'});

module.exports=mongoose.model('Course',CourseSchema);
