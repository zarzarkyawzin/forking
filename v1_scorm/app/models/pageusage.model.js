const mongoose=require('mongoose'),
    Schema =mongoose.Schema;

var PageUsageSchema=new Schema({
    itemId:{
        type: String
    },
    regId:{
        type: String
    },
    version:{
        type: Number,
        default: ()=>0
    },
    total_time:{
        type: String
    },
    first_access_date:{
        type: Date
    },
    last_access_date:{
        type: Date
    },
    complete_date:{
        type: Date
    },
    status:{
        type: String
    }
},{collection: 'PageUsage'});

module.exports=mongoose.model('PageUsage',PageUsageSchema);
