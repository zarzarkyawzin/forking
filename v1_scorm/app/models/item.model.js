const mongoose=require('mongoose'),
    Schema =mongoose.Schema;


var ItemSchema=new Schema({
    courseId: {
        type: String,
        required: true
    },
    version:{
        type: Number
    },
    parentId:{
        type: String
    },
    itemId:{
        type: String,
        required: true
    },
    itemTitle:{
        type: String
    }
    
},{collection: 'Item'});

module.exports=mongoose.model('Item',ItemSchema);
