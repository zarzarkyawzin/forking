const mongoose=require('mongoose'),
    Schema =mongoose.Schema;


var CourseUsageSchema=new Schema({
    regId: {
        type: String,
        required: true
    },
    courseId: {
        type: String,
        required: true
    },
    learnerId:{
        type: String,
        required:true
    },
    version:{
        type: Number,
        default: ()=>0
    },
    total_time:{
        type: Number
    },
    first_access_date:{
        type: Date
    },
    last_access_date:{
        type: Date
    },
    complete_date:{
        type: Date
    },
    status: {
        type: String
    },
    progress: {
        type: Number,
        default: 0
    }
    
},{collection: 'CourseUsage'});

module.exports=mongoose.model('CourseUsage',CourseUsageSchema);
