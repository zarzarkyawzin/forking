const mongoose=require('mongoose'),
    Schema =mongoose.Schema;


var PageRecordSchema=new Schema({
    version:{
        type: Number
    },
    itemId:{
        type: String,
        required:true
    },
    regId:{
        type: String,
        required:true
    },
    total_time:{
        type: Number,
        default:0
    },
    first_access_date:{
        type: Date,required:true,
        default:()=>Date.now()
    },
    last_access_date:{
        type: Date,
        default:null
    }
},{collection: 'PageDailyRecord'});

module.exports=mongoose.model('PageDailyRecord',PageRecordSchema);
